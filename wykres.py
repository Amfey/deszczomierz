# -*- coding: utf-8 -*-
import dbfread
#import numpy as np
import matplotlib.pyplot as plt
from matplotlib.dates import MonthLocator, DateFormatter
#import datetime
#import babel.dates
#import locale

#Ustawienia dat polskie
#locale.setlocale(locale.LC_TIME, "pl_PL.ISO8859-2") # polski

#Wczytujemy cały plik z deszczomierza
table = dbfread.read('2015.dbf')
#print type(table)

#tworzymy zmienne
DZIEN = list()
OPAD =list()
sum_day = 0
i = 0

#===============
#Testowe rzeczy
#dl = len(table)
#print table[1]['Suma opadu']
#==============

# Iterujemy po wszystkich wierszach
for wiersz in enumerate(table):

    d = wiersz[1]['DATE']
    #print table[i]['DATE']

    if d == table[i+1]['DATE']:
        #print 'Jest równy'
        if wiersz[1]['Suma opadu'] >= 0:
            sum_day = sum_day + wiersz[1]['Suma opadu']
            #print wiersz[1]['Suma opadu']

    else:
        #print 'Nie jest równy'
        DZIEN.append(d)
        OPAD.append(sum_day)
        #Zerujemy opad
        sum_day = 0

    i=i+1

    # Jeśli ostatni wiersz kończymy
    if i+1 == len(table):
        break


#print len(DZIEN), len (OPAD)

#===============================
#WCZYTUJEMY DRUGI ZESTAW DANYCH


#Wczytujemy cały plik z deszczomierza
table = dbfread.read('2016.dbf')
#print type(table)

#tworzymy zmienne
i = 0

# Iterujemy po wszystkich wierszach
for wiersz in enumerate(table):

    d = wiersz[1]['DATE']
    #print table[i]['DATE']

    if d == table[i+1]['DATE']:
        #print 'Jest równy'
        if wiersz[1]['Suma opadu'] >= 0:
            sum_day = sum_day + wiersz[1]['Suma opadu']
            #print wiersz[1]['Suma opadu']

    else:
        #print 'Nie jest równy'
        DZIEN.append(d)
        OPAD.append(sum_day)
        #Zerujemy opad
        sum_day = 0

    i=i+1

    # Jeśli ostatni wiersz kończymy
    if i+1 == len(table):
        break



#KONIEC DRUGIEGO ZESTAWU DANYCH
#===================

# Tworzymy wykres

plt.bar(DZIEN, OPAD)
plt.axhline(y=15, xmin=0, xmax=1, hold=None)

plt.show()

# Opcje wykresu

fig, ax = plt.subplots()

# Ustawiam formatowanie osi X
months = MonthLocator(range(1, 13), bymonthday=1, interval=1)
months_sr = MonthLocator(range(1, 13), bymonthday=15, interval=1)
monthsFmt = DateFormatter("")
months_sr_Fmt = DateFormatter("%B %y")

#====================
#Jak ustawić polskie nazwy
#months_sr_Fmt = babel.dates.format_date("%B %y", locale='pl_PL')
#=================


# Ustawiam tytuły osi
plt.xlabel('Data')
plt.ylabel('Opad [mm]')

#  Ustawiam znaczniki
ax.xaxis.set_major_locator(months)
ax.xaxis.set_major_formatter(monthsFmt)

# Wybieram etykiety aby były na środku
ax.xaxis.set_minor_locator(months_sr)
ax.xaxis.set_minor_formatter(months_sr_Fmt)

# Ustawiam kierunek znacznika
ax.tick_params(direction='out')

fig.autofmt_xdate()

#print datetime.today().date().strftime("%d %B %Y")
    # Iterujemy po wszystkich wierszach w tabeli
    #for key in table:

    #    if d == key['DATE']:
    #        d = key['DATE']
    #        sum_day = sum_day + key['Suma opadu']
    #    else
    #       DZIEN.append(d)
    #       OPAD.append(sum_day)

 # Tworzymy figure
 #fig = plt.figure()

 # Save'ujemy figure dla pythonanywhere
fig.savefig('graph.png')
